## **[requests](https://gitlab.com/Drumato/scraping)**

**画像一括ダウンロード**のツールを三種作った｡

**`getimagefromtxt.py`**…URLを一杯書いておいたテキストファイルから全部リクエスト送る｡  
**`crawl_abyss.py`**…クエリーパラメータも指定できるバージョン
**`sel.py`**…`Selenium`も使ってページ自動遷移も出来るようにしたやつ([こちらのサイト専用](https://wall.alphacoders.com/)

- `seluse`…`sel.py`からアクセスされる｡
- `gui`…`wxpython`を使ってWindowアプリを作った

起動はそれぞれ

```terminal
python <ファイル名>
```

とするだけ｡

**[詳細はこちらから](https://qiita.com/Drumato/items/8dcec37615312b705f07)**わかります｡