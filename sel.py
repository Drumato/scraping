from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import seluse
from seluse import for_sel_crawl
from bs4 import BeautifulSoup
import requests
import os
import sys
from time import sleep
import time
from datetime import datetime

url = seluse.get_url()

driver = webdriver.Chrome()
driver.get(url)

element = driver.find_element_by_name("search")

keyword = seluse.get_keyword()
element.send_keys(keyword)

element.submit()

cnt = 1
base_url = driver.current_url
while True:
    for_sel_crawl.download_site_images(url,'img/')
    cnt += 1
    url = f'{base_url}&page={cnt}'
    try:
        driver.get(url)
    except:
        sys.exit()
