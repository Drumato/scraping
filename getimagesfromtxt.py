# encoding: utf-8

import requests
from bs4 import BeautifulSoup
from datetime import datetime
import time
import os
import logzero
import timer
logger = logzero.setup_logger(
        name='scraping_logger',
        logfile='scrap.log',
        level=10,
        formatter=None,
        maxBytes=1000,
        backupCount=3,
        fileLoglevel=30,
        disableStderrLogger=False)

def download_site_images(url,path):

    if not os.path.exists(path):
        os.makedirs(path)

    soup = BeautifulSoup(url.content,'lxml')
    sources = [link.get("src") for link in soup.find_all('img')]
    images = [src for src in sources if src.split('.')[-1] in 'jpgpnggif']

    for image in images: #可読性のためList Comprehensionsは使わないがつかったほうが高速
        re = requests.get(image)
        logger.debug(f'Downloading:{image.split("/")[-1]}')
        with open(path + image.split('/')[-1],'wb') as f:
            f.write(re.content)

if __name__ == '__main__':
    now_time = datetime.now().strftime("%Y_%m_%d")
    with open('urlstore.txt') as urls:
        with timer.timer('processing'):
            urls = [url.rstrip() for url in urls]
            [download_site_images(requests.get(url),f'img/{now_time}') for url in urls]
