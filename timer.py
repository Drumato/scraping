from contextlib import contextmanager

import time
import logzero

logger = logzero.setup_logger(
        name='timer_logger',
        logfile='processTime.log',
        level=10,
        formatter=None,
        maxBytes=1000,
        backupCount=3,
        fileLoglevel=30,
        disableStderrLogger=False)

@contextmanager
def timer(phase):
    before = time.time()
    yield
    logger.debug(f'[{phase}] done in {time.time() - before:.3f}s')
