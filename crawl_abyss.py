# encoding: utf-8

import requests
from bs4 import BeautifulSoup
from datetime import datetime
import time
import os
import sys
from time import sleep

def download_site_images(url,path,query):
    images = []
    if not os.path.exists(path):
        os.makedirs(path)
    url = requests.get(url,params=query)
    soup = BeautifulSoup(url.content,'lxml')
    sources = [link.get("src") for link in soup.find_all('img')]
    images = [src for src in sources if src.split('.')[1] in 'jpgpnggif']
    for image in images:
        re = requests.get(image)
        print('Downloading:',image)
        with open(path + image.split('/')[-1],'wb') as f:
            f.write(re.content)


def get_url():
    uri = input("ダウンロードしたいサイトのURLを入力してください｡\n")
    return uri

def get_now_time():
    now_time = datetime.now().strftime("%Y_%m_%d")
    return now_time

def get_query():
    query = []
    params = []
    judge =  input("クエリー文字列を使用しますか?\n使用する場合はyesを入力\n")

    while True:
        if judge != 'yes':
            sys.exit()
        key = input("plz input words of query key\n")
        query.append(key)
        val = input("plz input words of query value\n")
        val = val.replace(" ","+")
      params.append(val)

        judge = input("do you want to continue inputting query?\nyes or no\n")

    dic = dict(zip(query,params))

    return dic



if __name__ == '__main__':
    judge = 'yes'
    while True:
        if judge != 'yes':
            sys.exit()
        url = get_url()
        now_time = get_now_time()
        query = get_query()
        download_site_images(url,f'img/{now_time}',query)

        judge = input("プログラムを継続しますか?\n継続する場合はyesを入力\n")
