# encoding:sjis 
import wx
from resources import crawl
import requests

def send_butfunc(event):
    url = requests.get(url_text.GetValue())
    crawl.download_site_images(url) 



application = wx.App()

frame = wx.Frame(None,wx.ID_ANY,'画像収集ツール',size=(640,480))

icon = wx.Icon('resources/Python.ico',wx.BITMAP_TYPE_ICO)
frame.SetIcon(icon)

layout = wx.FlexGridSizer(rows=3,cols=2,gap=(0,0))
menu_credit = wx.Menu()
menu_credit.Append(1,'作成者プロフィール')
menu_config = wx.Menu()
menu_config.Append(2,'クローリング設定')

menu_bar = wx.MenuBar()
menu_bar.Append(menu_config,'設定')
menu_bar.Append(menu_credit,'情報')

frame.SetMenuBar(menu_bar)

main_panel = wx.Panel(frame,wx.ID_ANY,pos=(0,0))

url_text = wx.TextCtrl(main_panel,wx.ID_ANY,size=(640,40),pos=(0,40))

surl_text = wx.StaticText(main_panel,wx.ID_ANY,'クローリング対象のURLを↓に入力')
font = wx.Font(20,wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL,wx.FONTWEIGHT_NORMAL)
surl_text.SetFont(font)
url_text.SetValue('URLを入力')

send_button = wx.Button(main_panel,wx.ID_ANY,'クロール開始',size=(100,100),pos=(0,80))
send_button.Bind(wx.EVT_BUTTON,send_butfunc)


frame.Centre()
frame.Show()


application.MainLoop()
