import requests
from bs4 import BeautifulSoup
from datetime import datetime
import os
import sys

def download_site_images(url):
	images = []

	if not os.path.exists('img/'):
		os.makedirs('img/')

	soup = BeautifulSoup(url.content,'lxml')

	for link in soup.find_all("img"):
		src = link.get("src")

		if 'jpg' in src:
			images.append(src)

		if 'png' in src:
			images.append(src)

		if 'gif' in src:
			images.append(src)

	for image in images:
		re = requests.get(image)
		print('Downloading:',image)
		with open('img/' + image.split('/')[-1],'wb') as f:
			f.write(re.content)
